import factory.SparkSessionFactory
import ml.ALSRecommender
import org.apache.spark.sql.SparkSession
import util.FileUtil

/**
  * Created by walanem.junior on 11/22/16.
  */
object Main {

  def main (args: Array[String]) = {

    val random = scala.util.Random
    val randomUserId = random.nextInt(101)

    val sparkSession: SparkSession = new SparkSessionFactory().createSession()
    val alsRecommender = new ALSRecommender(new FileUtil(sparkSession))

    alsRecommender.recommendGames(randomUserId)
  }
}