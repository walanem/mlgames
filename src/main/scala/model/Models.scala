package model

/**
  * Created by walanem.junior on 11/23/16.
  */
object Models {

  case class Game(id: Int,
                  score_phrase: String,
                  title: String,
                  url: String,
                  platform: String,
                  score: Double,
                  genre: String,
                  editors_choice: String,
                  release_year: Int,
                  release_month: Int,
                  release_day: Int)
  
  case class User(user_ID: Int,
                  user_Name: String)

  case class Friendship(user_Id: Int,
                        friend_Id: Int)
}