package factory

import org.apache.spark.sql.SparkSession
import javax.inject.Singleton

/**
  * Created by walanem.junior on 11/22/16.
  */
@Singleton
class SparkSessionFactory {

  private val APP_NAME = "MLGames"

  def createSession() : SparkSession = {

    val sparkSession = SparkSession.builder()
                                   .master("local[*]")
                                   .appName(APP_NAME)
                                   .getOrCreate()

    sparkSession
  }
}