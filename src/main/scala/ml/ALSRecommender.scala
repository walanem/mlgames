package ml

import javax.inject.{Inject, Singleton}

import factory.SparkSessionFactory
import model.Models.Friendship
import org.apache.spark.mllib.evaluation.{RankingMetrics, RegressionMetrics}
import org.apache.spark.mllib.recommendation.{ALS, MatrixFactorizationModel, Rating}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}
import util.{FileUtil, SchemaUtil}

/**
  * Created by walanem.junior on 11/23/16.
  */
@Singleton
class ALSRecommender @Inject()(fileUtil: FileUtil) extends Serializable {

  private val sparkSession: SparkSession = new SparkSessionFactory().createSession()

  private val MAX_RECOMMENDATIONS : Int = 5

  private val ALS_RANK : Int = 10         //  hidden features in the low-rank approximation matrix
  private val ALS_ITERATIONS : Int = 10   //  expected iteration number to converge the results
  private val ALS_LAMBDA : Double = 0.01  //  regularization parameter

  def recommendGames(userId: Int) = {

    import sparkSession.implicits._

    val schemaUtil = new SchemaUtil()

    val dfUsers = fileUtil.createDataframeFromFile(fileUtil.LOCAL_DEFAULT_PATH, fileUtil.USER_BASE_FILE, schemaUtil.createUserSchema())
    val dfGames = fileUtil.createDataframeFromFile(fileUtil.LOCAL_DEFAULT_PATH, fileUtil.GAMES_BASE_FILE, schemaUtil.createGameSchema())
    val dfRelationships = fileUtil.createDataframeFromFile(fileUtil.LOCAL_DEFAULT_PATH, fileUtil.RELATIONSHIP_BASE_FILE, schemaUtil.createRelationshipSchema())
    val dfRatings = fileUtil.createDataframeFromFile(fileUtil.LOCAL_DEFAULT_PATH, fileUtil.RATINGS_BASE_FILE, schemaUtil.createRatingSchema())

    // user friends
    val dfFriends = dfRelationships.filter(dfRelationships("user_Id") === userId)

    // ratings from friends
    val friends: Array[Friendship] = dfFriends.as[Friendship].collect()
    val friendIds : Array[Int] = for (x <- friends) yield x.friend_Id
    val friendIdsWithUser: Array[Int] = friendIds :+ userId   // why is it REALLY necessary?  ->  https://goo.gl/2mXz3Z
    val dfRatingsFromFriends = dfRatings.filter(dfRatings("user").isin(friendIdsWithUser.distinct.sorted : _*))

    // games from friends
    val ratings: Array[Rating] = dfRatingsFromFriends.as[Rating].collect()
    val gameIds : Array[Int] = for (x <- ratings) yield x.product
    val dfGamesFromFriends = dfGames.filter(dfGames("id").isin(gameIds.distinct.sorted : _*))

    // obtaining training/test data
    val ratingRDD: RDD[Rating] = obtainRatingRDD(dfRatingsFromFriends)
    val (trainData, testData) = processRecommendationDatasets(ratingRDD)

    // building an ALS model
    val model: MatrixFactorizationModel = createModel(trainData)

    runModel(model, userId, dfGamesFromFriends)

    // ----- Model Evaluation metrics -----
    evaluateModelbySortedMetrics(model, ratingRDD)      // Displaying results from different metrics
    evaluateModelbyMAP(model, ratingRDD)                // Displaying "Mean Average Precision" metric
    // ------------------------------------
  }

  private def obtainRatingRDD(dfRatingsFromFriends: DataFrame): RDD[Rating] = {

    import sparkSession.implicits._

    val dsRatings: Dataset[Rating] = dfRatingsFromFriends.as[Rating]
    val ratingsRDD = dsRatings.rdd

    return ratingsRDD
  }

  /**
    * Cross validation.
    *
    * Train a matrix factorization model given an RDD of 'implicit preferences' ratings.
    *
    * We have split the dataset into:
    *
    * train (80%)
    * test (20%)
    *
    * @param dsRating
    */
  private def processRecommendationDatasets(dsRating: RDD[Rating]) = {

    val splits = dsRating.randomSplit(Array(0.8, 0.2), 0L)

    val trainData = splits(0).cache()
    val testData = splits(1).cache()

    val numTraining = trainData.count()
    val numTest = testData.count()

    println(">>> Training set count: " + numTraining + "| Test set count: " + numTest)

    (trainData, testData)
  }

  /**
    * Responsible of building an (user x game) matrix features.
    *
    * @param trainData
    */
  private def createModel(trainData: RDD[Rating]): MatrixFactorizationModel = {

    val alsModel: MatrixFactorizationModel = ALS.train(trainData, ALS_RANK, ALS_ITERATIONS, ALS_LAMBDA)

    return alsModel
  }

  /**
    * Runs the model, applying the recommendation techniques.
    *
    * @param model
    * @param userId
    * @param dfGames
    * @return
    */
  private def runModel(model : MatrixFactorizationModel, userId: Int, dfGames: DataFrame) : DataFrame = {

    // Get the top game predictions for a specific user
    val recommendedRatings: Array[Rating] = model.recommendProducts(userId, MAX_RECOMMENDATIONS)
    println(recommendedRatings.sortBy(r => r.rating).mkString("\n"))

    // fetching game titles
    val gameIds : Array[Int] = for (r <- recommendedRatings) yield r.product
    val filteredGames = dfGames.filter(dfGames("id").isin(gameIds : _*))

    // print out top game recommendations for user with it's titles
    println("### Top recommended games for you, based on friends choices ###")
    filteredGames.select("title").foreach(row => println(row.get(0)))

    filteredGames
  }

  /**
    * Evaluates the model by using Mean Absolute Error metric.
    *
    * @param model
    * @param testData
    * @return
    */
  private def evaluateModelbyMAE(model: MatrixFactorizationModel, testData: RDD[Rating]) = {

    // get user product pair from testRatings
    val testUserProductRDD = testData.map {
      case Rating(user, product, rating) => (user, product)
    }

    // get predicted ratings to compare to test ratings
    val predictionsForTestRDD  = model.predict(testUserProductRDD)
    predictionsForTestRDD.take(10).mkString("\n")

    // prepare predictions for comparison
    val predictionsKeyedByUserProductRDD = predictionsForTestRDD.map{
      case Rating(user, product, rating) => ((user, product), rating)
    }

    // prepare test for comparison
    val testKeyedByUserProductRDD = testData.map{
      case Rating(user, product, rating) => ((user, product), rating)
    }

    //Join the test with predictions
    val testAndPredictionsJoinedRDD = testKeyedByUserProductRDD.join(predictionsKeyedByUserProductRDD)

    // print the (user, product),( test rating, predicted rating)
    testAndPredictionsJoinedRDD.take(3).mkString("\n")

    // false positives test.
    //
    // matches results among predicted ratings (>=4 is mostly recommended)
    val falsePositives = (
      testAndPredictionsJoinedRDD.filter{
        case ((user, product), (ratingTest, ratingPrediction)) => (ratingTest <= 1 && ratingPrediction >=4)
    })

    falsePositives.take(2)
    falsePositives.count()

    // Evaluate the model using Mean Absolute Error (MAE) between test.
    // It shows the absolute differences between the predicted and actual targets.
    val meanAbsoluteError = testAndPredictionsJoinedRDD.map {
      case ((user, product), (testRating, predictionRating)) =>
        val error = (testRating - predictionRating)
        Math.abs(error)
    }.mean()

    println(">>> Mean absolute error (MAE): " + meanAbsoluteError)
  }

  /**
    * Evaluates the model by using the following metrics:
    *
    * - Mean Squared Error
    * - Root Mean Squared Error
    * - Mean Absolute Error
    *
    * @param model
    *
    */
  private def evaluateModelbySortedMetrics(model: MatrixFactorizationModel, ratings: RDD[Rating]) = {

    // Extracting the user and product IDs from the ratings RDD.
    val usersProducts = ratings.map{ case Rating(user, product, rating)  => (user, product)}

    // Then, we make predictions for each (user x item) pair.
    val predictions = model.predict(usersProducts).map{
      case Rating(user, product, rating) => ((user, product), rating)
    }

    // Extracting the actual ratings and also mapping the ratings RDD
    // That way, the (user x item) pair is the key and the actual rating is the value.
    //
    // Now that the two RDDs have the same form of key, we can join them together
    // to create a new RDD with the actual and predicted ratings for each user-item combination.
    val ratingsAndPredictions = ratings.map{
      case Rating(user, product, rating) => ((user, product), rating)
    }.join(predictions)

    val predictedAndTrue = ratingsAndPredictions.map {
      case ((user, product), (predicted, actual)) => (predicted, actual)
    }

    val rm = new RegressionMetrics(predictedAndTrue)

    val MSE: Double = rm.meanSquaredError

    // Finally, we calculate the value of RMSE.
    // It is equivalent to the standard deviation of the differences between the predicted and actual ratings.
    val RMSE: Double = rm.rootMeanSquaredError

    println(">>> Mean Squared Error (MSE) = " + MSE)
    println(">>> Root Mean Squared Error (RMSE) = " + RMSE)
    println(">>> Mean Absolute Error (MAE) = " + rm.meanAbsoluteError)
  }

  /**
    * Evaluates the model by using Mean Average Precision (MAP) metric.
    */
  private def evaluateModelbyMAP(model: MatrixFactorizationModel, ratings: RDD[Rating]) = {

    // Map ratings to 1 or 0, 1 indicating a movie that should be recommended
    val binarizedRatings = ratings.map(r => Rating(r.user, r.product,
      if (r.rating > 0) 1.0 else 0.0)).cache()

    // Define a function to scale ratings from 0 to 1
    def scaledRating(r: Rating): Rating = {
      val scaledRating = math.max(math.min(r.rating, 1.0), 0.0)
      Rating(r.user, r.product, scaledRating)
    }

    // Get sorted top predictions for each user and then scale from [0, 1]
    val userRecommended = model.recommendProductsForUsers(MAX_RECOMMENDATIONS).map {
      case (user, recs) => (user, recs.map(scaledRating))
    }

    // Assume that any movie a user rated 3 or higher (which maps to a 1) is a relevant document
    // Compare with top ten most relevant documents
    val userMovies = binarizedRatings.groupBy(_.user)
    val relevantDocuments = userMovies.join(userRecommended).map { case (user, (actual,
    predictions)) =>
      (predictions.map(_.product), actual.filter(_.rating > 0.0).map(_.product).toArray)
    }

    val rm = new RankingMetrics(relevantDocuments)

    println(">>> Mean Average Precision (MAP) = " + rm.meanAveragePrecision)
  }
}