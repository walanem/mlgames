package util

import java.io.File

import model.Models.{Friendship, Game, User}
import org.apache.spark.mllib.recommendation.Rating

import scala.io.Source

/**
  * Created by walanem.junior on 11/23/16.
  */
class Parser {

  /**
    * Creates a list of users based on input dataset
    *
    * @param path
    * @return
    */
  def parseUsers(path: String) = {

    for (line <- Source.fromFile(path).getLines()) yield {
      val Array(user_ID, user_Name) = line.split(";")
      User(user_ID.toInt, user_Name)
    }
  }

  /**
    * Creates a list of user friendships based on input dataset
    *
    * @param path
    * @return
    */
  def parseFriendships(path: String) = {

    for (line <- Source.fromFile(path).getLines()) yield {
      val Array(user_Id, friend_Id) = line.split(";")
      Friendship(user_Id.toInt, friend_Id.toInt)
    }
  }

  /**
    * Creates a list of user ratings
    *
    * @param path
    * @return
    */
  def parseRatings(path: String) = {

    for (line <- Source.fromFile(path).getLines()) yield {
      val Array(user_Id, game_ID, user_Rating) = line.split(";")
      Rating(user_Id.toInt, game_ID.toInt, user_Rating.toInt)
    }
  }

  /**
    * Creates a list of users based on input dataset
    *
    * @param path
    * @return
    */
  def parseGames(path: String) = {

    for (line <- Source.fromFile(path).getLines()) yield {
      val Array(id, score_phrase, title, url, platform, score, genre, editors_choice, release_year, release_month, release_day) = line.split(",")
      Game(id.toInt, score_phrase, title, url, platform, score.toDouble, genre, editors_choice, release_year.toInt, release_month.toInt, release_day.toInt)
    }
  }
}