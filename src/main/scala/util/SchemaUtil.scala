package util

import org.apache.spark.sql.types.{DataTypes, StructField, StructType}

/**
  * Created by walanem.junior on 12/12/16.
  */
class SchemaUtil {

  def createUserSchema() = {

    val user_ID = StructField("user_ID", DataTypes.IntegerType)
    val user_Name = StructField("user_Name", DataTypes.StringType)
    val fields = Array(user_ID, user_Name)
    val userSchema = StructType(fields)

    userSchema
  }

  def createGameSchema() = {

    val id = StructField("id", DataTypes.IntegerType)
    val score_phrase = StructField("score_phrase", DataTypes.StringType)
    val title = StructField("title", DataTypes.StringType)
    val url = StructField("url", DataTypes.StringType)
    val platform = StructField("platform", DataTypes.StringType)
    val score = StructField("score", DataTypes.DoubleType)
    val genre = StructField("genre", DataTypes.StringType)
    val editors_choice = StructField("editors_choice", DataTypes.StringType)
    val release_year = StructField("release_year", DataTypes.IntegerType)
    val release_month = StructField("release_month", DataTypes.IntegerType)
    val release_day = StructField("release_day", DataTypes.IntegerType)
    val fields = Array(id, score_phrase, title, url, platform, score, genre, editors_choice, release_year, release_month, release_day)
    val gameSchema = StructType(fields)

    gameSchema
  }

  def createRelationshipSchema() = {

    val user_Id = StructField("user_Id", DataTypes.IntegerType)
    val friend_Id = StructField("friend_Id", DataTypes.IntegerType)
    val fields = Array(user_Id, friend_Id)
    val relationshipSchema = StructType(fields)

    relationshipSchema
  }

  def createRatingSchema() = {

    val user = StructField("user", DataTypes.IntegerType)
    val product = StructField("product", DataTypes.IntegerType)
    val rating = StructField("rating", DataTypes.IntegerType)
    val fields = Array(user, product, rating)
    val ratingSchema = StructType(fields)

    ratingSchema
  }
}
