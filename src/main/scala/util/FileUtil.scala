package util

import javax.inject.{Inject, Singleton}

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StructType

/**
  * Created by walanem.junior on 11/22/16.
  */
@Singleton
class FileUtil @Inject()(sparkSession: SparkSession) extends Serializable {

  val HDFS_DEFAULT_PATH : String = "hdfs://%s/mlgames_datasets/"
  val HDFS_DEFAULT_HOST : String = "localhost"
  val LOCAL_DEFAULT_PATH : String = "/home/walanem.junior/Desktop/datasets/"

  val GAMES_BASE_FILE : String = "game_base.csv"
  val RATINGS_BASE_FILE : String = "rating_base.csv"
  val RELATIONSHIP_BASE_FILE : String = "relationship_base.csv"
  val USER_BASE_FILE : String = "user_base.csv"

  def createDataframeFromFile(basePath: String, fileName: String, schema: StructType) = {

    val df = sparkSession.read
                         .option("header", true)
                         .option("delimiter", ";")
                         .schema(schema)
                         .csv(basePath.concat(fileName))
//                         .csv(basePath.format(HDFS_DEFAULT_HOST).concat(fileName))

    df
  }
}